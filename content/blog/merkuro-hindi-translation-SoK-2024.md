---
title: "Merkuro Hindi Translation - SoK 2024"
date: 2024-02-17T13:05:55+05:30
---

This is my first blog about my experience participating in season of KDE 2024.

![season of kde logo](/season-of-kde.png)

Season of KDE is a mentorship program in which mentees work on project ideas offered by mentors. I came to know about Season of KDE in November of 2023 through a youtube video. I applied and got selected for the translation project.

I also did some translations of small applications which included kcharselect, kruler and ktimer, as a pre-task. My work can be found at https://github.com/AkashKumar7902/translations-kde-applications

In my proposal, I choose the task to translate merkuro and tellico applications to hindi. Tellico and merkuro are one of the most starred projects among kde projects. Tellico is used by thousands of people around the world to manage collections of books, videos, music, etc. While, merkuro provides a simple way to handle emails, calendars and contacts. This project aims to translate all sentences displayed to the user into Hindi.

In the first phase of my work, I translated **merkuro**. 

I used lokalize as a translation tool. Lokalize provides a intutive and simple UI, with awesome features like translation memory. To setup lokalize, I followed this blog by Raghavendra Kamath: https://raghukamath.com/how-to-translate-krita-to-your-own-language. 

The below diagram outlines the workflow followed for translation.
![translation workflow](/workflow.png)

The translation work went quite smoothly, I hardly faced any challenges. However, it's important to exercise caution when deciding whether to transliterate or simply translate.

The translations can be found at https://invent.kde.org/akashkumar/sok-translations/-/tree/master/merkuro/messages?ref_type=heads

I would like to thank my mentors, Benson Muite and Raghavendra Kamath for providing exceptional support. I would also like to thank Aakarsh MJ for taking up the work to review merkuro translation.

This is all for this blog. I would be posting another blog describing my experience translating tellico. 

Stay Tuned !  